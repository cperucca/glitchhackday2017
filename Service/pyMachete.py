import os
import subprocess
import uuid
import codecs
import tempfile


def Taglia_File_Tmp( file_in, tc_in, tc_out):

	print ( 'inizio Taglia')
	# ocio che output funziona con la estensione
	temp_name = './Chops/' + next(tempfile._get_candidate_names()) +'.mp4'
	print (temp_name)

	command = 'ffmpeg -y -i ' + file_in + ' -ss ' + tc_in + ' -to ' + tc_out + ' -c copy  ' + temp_name
	print ( 'inizio ' + command)

	p = subprocess.Popen(command, shell=True,
			     stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	print ( 'dopo Popen ')
	output = p.communicate()[0]
	print ( ' FINE  Taglia' + str(output))
	return temp_name



def Taglia( file_in, tc_in, tc_out, file_out):

	print ( 'inizio Taglia')
	command = 'ffmpeg -y -i ' + file_in + ' -ss ' + tc_in + ' -to ' + tc_out + ' -c copy  ' + file_out
	print ( 'inizio ' + command)

	p = subprocess.Popen(command, shell=True,
			     stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	print ( 'dopo Popen ')
	output = p.communicate()[0]
	print ( ' FINE  Taglia' + str(output))


def Incolla( lista_files_in, file_out ):
	
	print ( 'inizio Incolla')
	
	# creiamo il file dei tagli da effettuare
	# il nome file deve essere univocoo
	temp_name = next(tempfile._get_candidate_names())
	print (temp_name)

	file_lista_file = codecs.open( temp_name, 'w', 'utf-8')
	
	for lis in lista_files_in:
		print (' lis ' + lis)
		file_lista_file.write('file \'' + lis +'\'\n')

	file_lista_file.close()
	
		
	command = 'ffmpeg -y -f concat -i ' + temp_name + ' -c copy "' + file_out + '"'
	print ( 'inizio ' + command)

	p = subprocess.Popen(command, shell=True,
			     stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	print ( 'dopo Popen ')
	output = p.communicate()[0]

	os.remove( temp_name )
	print ( ' FINE  Incolla' + str(output))



if __name__ == "__main__":

	#Taglia('./Resources/391429_2279014.mp4', '00:00:25', '00:00:35', './taglio_1.mp4')
	#Taglia('./Resources/391429_2279014.mp4', '00:01:00', '00:01:40', './taglio_2.mp4')

	Taglia('./Resources/Noise.mp4', '00:00:10', '00:00:10.50', './Resources/Taglio_Glitch_2.mp4')

	#Incolla( ['./taglio_1.mp4', './Resources/Taglio_Glitch.mp4', './taglio_2.mp4', './Resources/Taglio_Glitch.mp4.mp4' ] , './IncollaVideo.mp4')
