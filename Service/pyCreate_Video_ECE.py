import urllib, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import shutil
import glob
import json
import subprocess
import tempfile

from urllib.parse import urlencode
from urllib.request import Request, urlopen

TEMPLATE_DIR = '/home/perucccl/HackDay2017/Templates'
CHOPS_DIR = '/home/perucccl/HackDay2017/Chops'

def Import_Bin( filename ):


	#prendo il file generato e lo uploado con
	#curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml" --upload-file NOME_FILE  http://internal.publishing.production.rsi.ch/webservice/escenic/binary

	print ( 'inizio Taglia')
	command = 'curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml" --upload-file ' + filename + ' http://internal.publishing.production.rsi.ch/webservice/escenic/binary'

	print ( 'inizio ' + command)

	p = subprocess.Popen(command, shell=True,
			     stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	#print ( 'dopo Popen ')
	output = p.communicate()[0]
	#print ( ' FINE  Taglia' + str(output))

	result =  str(output).replace('\\r\\n',';')
	#print ( result)

	result =  str(result).split(';')
	#print ( result)
	#print ( result[-6][10:])
	return result[-6][10:]


def Invia_Template_Xml( filename ):


	#prendo il file generato e lo uploado con
	#curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml" 'http://internal.publishing.production.rsi.ch/webservice/escenic/section/5909/content-items' --upload-file TEMPLATE_TRANSCODABLE.xml
	#Ocio alla sezione che sia quella giusta.

	print ( 'inizio Create_Video')
	command = 'curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  "http://internal.publishing.production.rsi.ch/webservice/escenic/section/5909/content-items" --upload-file ' + filename

	print ( 'inizio ' + command)

	p = subprocess.Popen(command, shell=True,
			     stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	#print ( 'dopo Popen ')
	output = p.communicate()[0]
	#print ( ' FINE  Taglia' + str(output))

	result =  str(output).replace('\\r\\n',';')
	result =  str(result).split(';')
	#print ( result)
	#print ( result[-5][10:].split('content/')[-1] )
	return result[-5][10:].split('content/')[-1]

def Crea_Video_ECE( filename, titolo ):

	bin_url = Import_Bin( filename )
	print (bin_url)
	

	#bin_url = 'http://internal.publishing.production.rsi.ch/webservice/escenic/binary/-1/2017/1/30/16/bc6d0520-06ed-4099-93f2-3a479a80508d.bin'

	#che e' quello che devo mettere editando il TEMPLATE_TRANSCODABLE.xml
	#rimpiazzando il valore "__REPLACE_BIN_URL__" con bin_url
	#e mettendolo nel field binary assieme a titolo "__REPLACE_TITLE__"
	#e altre amenita'
	#incluso il nome del file di sottotitoli nel campo GoogleVideoDescription "__REPLACE_PATH_SUBTITLES__"
	fin_template = codecs.open( TEMPLATE_DIR + '/TEMPLATE_TRANSCODABLE.xml', 'r', 'utf-8' )
	filename_out = CHOPS_DIR +  '/' + next(tempfile._get_candidate_names()) + '.xml'
	fout_result = codecs.open( filename_out, 'w', 'utf-8')

	# nome_sottotitoli = ts_DATE_i_ECEID.xml
	nome_sottotitoli = 'Ackdei'

	lines = fin_template.readlines()
	for line in lines:
		fout_result.write( line.replace("__REPLACE_BIN_URL__", bin_url).replace("__REPLACE_TITLE__", titolo).replace("__REPLACE_PATH_SUBTITLES__", nome_sottotitoli))

	
	fin_template.close()
	fout_result.close()
	
	#che poi uploado sulla url
	#curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml" 'http://internal.publishing2.production.rsi.ch/webservice/escenic/section/5909/content-items' --upload-file TEMPLATE_TRANSCODABLE.xml
	#Ocio alla sezione che sia quella giusta.
	result = Invia_Template_Xml( filename_out )
	print (result)
	os.remove( filename_out )
	return result

	

if __name__ == "__main__":


	Crea_Video_ECE( '../Resources/test_out.mp4' , 'Titolo Esempio' )

